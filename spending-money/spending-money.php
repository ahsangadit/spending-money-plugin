<?php
/**
 * Plugin Name: Spending Money
 * Plugin URI:
 * Description: Provide authentication support in wordpress posts and pages.
 * Version: 1.0.0
 * Author: HZTech
 * Author URI: http://hztech.biz
 **/


if ( ! define( 'SPENDINGMONEY_PLUGIN_VERSION', '1.0.0' ) ) {
	define( 'SPENDINGMONEY_PLUGIN_VERSION', '1.0.0' );
}


if ( ! define( 'SPENDINGMONEY_PLUGIN_DIR', plugin_dir_path( __FILE__ ) ) ) {
	define( 'SPENDINGMONEY_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
}

if ( ! function_exists( 'wp_handle_upload' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/file.php' );
}


if ( ! function_exists( 'spendingMoney_plugin_script' ) ) {
    function spendingMoney_plugin_script()
    {
        wp_enqueue_script( 'spendingMoney-js', plugins_url( 'assets/js/jquery-master-number.js', __FILE__ ), array( 'jquery' ), rand( 0, 9999 ), true );
    }

    add_action( 'wp_enqueue_scripts', 'spendingMoney_plugin_script' );

}

/* Register  Pages */
include( SPENDINGMONEY_PLUGIN_DIR . 'inc/admin-pages/settingPage.php' );

/*Custom Meta Box For Posts*/
include( SPENDINGMONEY_PLUGIN_DIR . 'inc/custom-fields/custom-meta-fields.php' );

add_filter( 'manage_page_posts_columns', 'smashing_CountyProtected_posts_columns' );
function smashing_CountyProtected_posts_columns( $columns )
{
	$columns['lock'] = __( 'Lock' );

	return $columns;
}

add_action( 'manage_page_posts_custom_column', 'smashing_CountyProtected_column', 10, 2 );
function smashing_CountyProtected_column( $column, $post_id )
{
	// Lock column

	if ( 'lock' === $column ) {
		$content = get_post_meta( $post_id, "countyProtected_", true );

		if ( ( $content == "restricted" ) ) {
			echo "Yes";
		} else {
			echo "No";
		}
	}

}

if ( ! class_exists( 'SpendingMoney' ) ) {
	include( SPENDINGMONEY_PLUGIN_DIR . 'inc/functions.php' );
}

register_activation_hook( __FILE__, 'my_plugin_activate' );

function my_plugin_activate()
{

///* BAR PAGE */
//
	$page = array(
		'post_title'   => 'Bar Page',
		'post_content' => '[bar_shortcode]',
		'post_status'  => 'publish',
		'post_type'    => 'page',
	);

	$id = wp_insert_post( $page );
	update_option( 'bar_page', $id );
}
