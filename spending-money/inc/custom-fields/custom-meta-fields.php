<?php

/**
 * Register meta boxes.
 */
function hcf_register_meta_boxes()
{
	add_meta_box( 'hcf-1', __( 'Status', 'hcf' ), 'hcf_display_callback_protectCheckbox_label', [
		'Programs'
	], 'side' );

	add_meta_box( 'hcf-1', __( 'Form Detail', 'hcf' ), 'hcf_display_callback_Form_Detail', [
		'Form Details'
	], 'normal' );

	add_meta_box( 'hcf-1', __( 'Entries', 'hcf' ), 'hcf_display_callback_Entries', [
		'Entries'
	], 'normal' );
}

add_action( 'add_meta_boxes', 'hcf_register_meta_boxes' );

/**
 * Meta box display callback.
 *
 * @param WP_Post $post Current post object.
 */
function hcf_display_callback_protectCheckbox_label( $post )
{

	$content = get_post_meta( get_the_ID(), 'status_', true );
	$colour  = get_post_meta( get_the_ID(), 'colour', true );

	?>
    <div class="hcf_box">
        <style scoped>
            .hcf_box {
                display: grid;
                grid-template-columns: max-content 1fr;
                grid-row-gap: 10px;
                grid-column-gap: 20px;
                margin: 10px 0;
            }

            .hcf_field {
                display: contents;
            }

        </style>
        <p class="meta-options hcf_field">
            <label for="hcf_author">Active</label>
            <input type="checkbox"
                   name="status_"
                   id="status_"
                   value="restricted"
				<?php if ( $content == "restricted" ) {
					echo esc_attr( "checked" );
				} else {
					echo esc_attr( null );
				} ?>
            />
        </p>

        <p class="meta-options hcf_field">
            <label for="hcf_author">Bars Colour</label>
            <input type="color"
                   name="colour"
                   id="colour"
                   value="<?= $colour ?>"
            />
        </p>
    </div>
	<?php
}


function hcf_display_callback_Form_Detail( $post )
{

	$args          = array(
		'post_type'      => 'programs',
		'post_status'    => 'publish',
		'posts_per_page' => - 1,
	);
	$programs_post = get_posts( $args );

	// var_dump(get_the_ID());
	$programs            = get_post_meta( get_the_ID(), 'programs', true );
	$total_points        = get_post_meta( get_the_ID(), 'total_points', true );
	$no_of_points        = get_post_meta( get_the_ID(), 'no_of_points', true );
	$b_colour            = get_post_meta( get_the_ID(), 'b_colour', true );
	$inc_buttons         = get_post_meta( get_the_ID(), 'inc_buttons', true );
	$coin_active         = get_post_meta( get_the_ID(), 'coin_active', true );
	$coin_inactive       = get_post_meta( get_the_ID(), 'coin_inactive', true );
	$submit_colour       = get_post_meta( get_the_ID(), 'submit_colour', true );
	$unserialize_program = unserialize( $programs );

	if ( empty( $unserialize_program ) ) {
		$unserialize_program = [];
	}

	?>
    <div class="hcf_box">
        <style scoped>
            .hcf_box {
                display: grid;
                grid-template-columns: max-content 1fr;
                grid-row-gap: 10px;
                grid-column-gap: 20px;
                margin: 10px 0;
            }

            .hcf_field {
                display: contents;
            }

        </style>
        <p class="meta-options hcf_field">
            <label>Programs: </label>
            <select name="programs[]" id="programs" multiple>
				<?php foreach ( $programs_post as $data ) { ?>
                    <option value="<?= $data->ID ?>" <?php if ( in_array( $data->ID, $unserialize_program ) ) {
						echo 'selected="selected"';
					} ?> > <?= $data->post_title ?> </option>
				<?php } ?>
            </select>
        </p>
        <p class="meta-options hcf_field">
            <label>Total Points: </label>
            <input type="text" class="total_points" name="total_points" value="<?= $total_points ?>"/>
        </p>
        <p class="meta-options hcf_field">
            <label>No Of Points</label>
            <input type="text" class="no_of_points" name="no_of_points" value="<?= $no_of_points ?>"/>
        </p>
        <p class="meta-options hcf_field">
            <label>Background Colour :</label>
            <input type="color" class="b_colour" name="b_colour" value="<?= $b_colour ?>"/>
        </p>
        <p class="meta-options hcf_field">
            <label>Buttons</label>
            <input type="color" class="inc_buttons" name="inc_buttons" value="<?= $inc_buttons ?>"/>
        </p>
        <p class="meta-options hcf_field">
            <label>Coin Active</label>
            <input type="color" class="coin_active" name="coin_active" value="<?= $coin_active ?>"/>
        </p>
        <p class="meta-options hcf_field">
            <label>Coin Inactive</label>
            <input type="color" class="coin_inactive" name="coin_inactive" value="<?= $coin_inactive ?>"/>
        </p>
        <p class="meta-options hcf_field">
            <label>Submit Colour</label>
            <input type="color" class="submit_colour" name="submit_colour" value="<?= $submit_colour ?>"/>
        </p>
    </div>
	<?php
}


function hcf_display_callback_Entries( $post )
{

	$args = array(
		'post_type'      => 'entries',
		'post_status'    => 'publish',
		'posts_per_page' => - 1,
	);


	?>
    <div class="hcf_box">
        <style scoped>
            .hcf_box {
                display: grid;
                grid-template-columns: max-content 1fr;
                grid-row-gap: 10px;
                grid-column-gap: 20px;
                margin: 10px 0;
            }

            .hcf_field {
                display: contents;
            }

        </style>
		<?php
		$args     = array(
			'post_type'      => 'programs',
			'post_status'    => 'publish',
			'posts_per_page' => - 1,
		);
		$programs = get_posts( $args );

		$form_name = get_post_meta( get_the_ID(), 'form_name', true );

		foreach ( $programs as $data ) {
			$programss = get_post_meta( get_the_ID(), $data->post_title, true );
			?>
            <p class="meta-options hcf_field">
                <label><?= $data->post_title ?></label>
                <input type="text" class="no_of_points" name="<?= $data->post_title ?>" value="<?= $programss ?>"
                       disabled/>
            </p>
		<?php } ?>
        <p class="meta-options hcf_field">
            <label>Form Id</label>
            <input type="text" class="form_id" name="form_id" value="<?= $form_name ?>" disabled/>
        </p>
    </div>
	<?php
}


/*
 *Save meta box content.
 *
 * @param int $post_id Post ID
*/

function hcf_save_meta_box_countyProtect_checkbox( $post_id )
{
	global $post;
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	if ( $parent_id = wp_is_post_revision( $post_id ) ) {
		$post_id = $parent_id;
	}

	if ( ! empty( $post ) && $post->post_type == 'formdetails' ) {
		update_post_meta( $post_id, 'programs', serialize( $_POST['programs'] ) );
		update_post_meta( $post_id, 'total_points', sanitize_text_field( $_POST['total_points'] ) );
		update_post_meta( $post_id, 'no_of_points', sanitize_text_field( $_POST['no_of_points'] ) );
		update_post_meta( $post_id, 'inc_buttons', sanitize_text_field( $_POST['inc_buttons'] ) );
		update_post_meta( $post_id, 'coin_active', sanitize_text_field( $_POST['coin_active'] ) );
		update_post_meta( $post_id, 'coin_inactive', sanitize_text_field( $_POST['coin_inactive'] ) );
		update_post_meta( $post_id, 'b_colour', sanitize_text_field( $_POST['b_colour'] ) );
		update_post_meta( $post_id, 'submit_colour', sanitize_text_field( $_POST['submit_colour'] ) );
	}


	if ( ! empty( $post ) && $post->post_type == 'programs' ) {
		update_post_meta( $post_id, 'status_', sanitize_text_field( $_POST['status_'] ) );
		update_post_meta( $post_id, 'colour', sanitize_text_field( $_POST['colour'] ) );

	}

	if ( ! empty( $post ) && $post->post_type == 'entries' ) {
		$args     = array(
			'post_type'      => 'programs',
			'post_status'    => 'publish',
			'posts_per_page' => - 1,
		);
		$programs = get_posts( $args );
		foreach ( $programs as $data ) {
			update_post_meta( $post_id, $data->post_title, $_POST[ $data->post_title ] );
		}

	}

}

add_action( 'save_post', 'hcf_save_meta_box_countyProtect_checkbox' );
