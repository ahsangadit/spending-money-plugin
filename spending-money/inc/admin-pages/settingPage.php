<?php


function spendingMoneyProtected_register_menu_page()
{
	add_menu_page( 'Spending Money', 'Spending Money', 'manage_options', 'spending-money-settings', 'spendingMoney_settings_page_html', 'dashicons-post-status', 30 );


}

add_action( 'admin_menu', 'spendingMoneyProtected_register_menu_page' );


function spendingMoney_plugin_settings()
{

	$current_url = home_url() . '/wp-admin/admin.php?page=spending-money-settings';

	add_option( 'spendingMoney_url', $current_url );
	register_setting( 'spendingMoney-settings', 'spendingMoney_client_id' );
	register_setting( 'spendingMoney-settings', 'spendingMoney_client_secret' );
	register_setting( 'spendingMoney-settings', 'spendingMoney_access_token' );
	$access_token = get_option( 'spendingMoney_access_token' );


	add_settings_section( 'spendingMoney_label_setting_section', 'Login Auth Settings', 'spendingMoney_plugin_setting_section_callback', 'spendingMoney-settings' );


	add_settings_field( 'spendingMoney_loginPage_field', 'Client ID', 'spendingMoney_plugin_client_id_field_callback', 'spendingMoney-settings'
		, 'spendingMoney_label_setting_section'
	);

	add_settings_field( 'spendingMoney_field', 'Client Secret', 'spendingMoney_plugin_client_secret_field_callback', 'spendingMoney-settings'
		, 'spendingMoney_label_setting_section'
	);
	add_settings_field( 'spendingMoney_field_btn', 'Access Token', 'spendingMoney_plugin_setting_btn_field_callback', 'spendingMoney-settings'
		, 'spendingMoney_label_setting_section'
	);


}

function spendingMoney_plugin_setting_section_callback()
{

}

add_action( 'admin_init', 'spendingMoney_plugin_settings' );

function spendingMoney_plugin_client_id_field_callback()
{
	// get the value of the setting we've registered with register_setting()
	$client_id = get_option( 'spendingMoney_client_id' );
	?>
    <input type="text" name="spendingMoney_client_id" id="spendingMoney_client_id"
           value="<?php echo isset( $client_id ) ? esc_attr( $client_id ) : null; ?>" style="width: 300px;">
	<?php

}

function spendingMoney_plugin_client_secret_field_callback()
{

	// get the value of the setting we've registered with register_setting()
	$client_secret = get_option( 'spendingMoney_client_secret' );
	?>
    <input type="text" name="spendingMoney_client_secret" id="spendingMoney_client_secret"
           value="<?php echo isset( $client_secret ) ? esc_attr( $client_secret ) : null; ?>" style="width: 300px;">
	<?php
}

function spendingMoney_plugin_access_token_field_callback()
{

	// get the value of the setting we've registered with register_setting()
	$client_secret = get_option( 'spendingMoney_access_token' );
	?>
    <input type="text" name="spendingMoney_access_token" id="spendingMoney_access_token"
           value="<?php echo isset( $client_secret ) ? esc_attr( $client_secret ) : null; ?>" style="width: 300px;">
	<?php
}

function spendingMoney_plugin_setting_btn_field_callback()
{
	$client_secret = get_option( 'spendingMoney_client_secret' );
	$client_id     = get_option( 'spendingMoney_client_id' );
	$access_token  = get_option( 'spendingMoney_access_token' );
	$redirect_url  = get_option( 'spendingMoney_url' );

	if ( $client_id != '' && $client_secret != '' && $access_token == "" ) {
		$login_url = 'https://accounts.google.com/o/oauth2/auth?scope=' . urlencode( 'https://www.googleapis.com/auth/drive.file' ) . '&redirect_uri=' . urlencode( $redirect_url ) . '&response_type=code&client_id=' . $client_id . '&access_type=online';
		?>
        <a id="logo" href="<?= $login_url ?>">Login with Google</a>
	<?php } else if ( $access_token != "" ) {
		?>
        <input type="text" id="spendingMoney_client_secret"
               value="<?php echo isset( $access_token ) ? esc_attr( $access_token ) : null; ?>" style="width: 300px;">
	<?php }
}


function spendingMoney_settings_page_html()
{


	if ( ! is_admin() ) {
		return;
	}
	if ( isset( $_GET['code'] ) ) {
		include_once( SPENDINGMONEY_PLUGIN_DIR . 'inc/GoogleSpreadsheetsApi.php' );
		try {
			$sapi          = new GoogleSpreadsheetsApi();
			$client_secret = get_option( 'spendingMoney_client_secret' );
			$client_id     = get_option( 'spendingMoney_client_id' );
			$redirect_url  = get_option( 'spendingMoney_url' );
			// Get the access token
			$data = $sapi->GetAccessToken( $client_id, $redirect_url, $client_secret, $_GET['code'] );

			// Save the access token as a session variable
			update_option( 'spendingMoney_access_token', $data['access_token'] );

			// Redirect to the page where user can create spreadsheet
			wp_redirect( $redirect_url );
		} catch ( Exception $e ) {
			echo $e->getMessage();
			exit();
		}
	}
	?>
    <div class="wrap">
        <form action="options.php" method="post" enctype="multipart/form-data">
			<?php
			settings_fields( 'spendingMoney-settings' );
			do_settings_sections( 'spendingMoney-settings' );
			submit_button( 'Save Changes' );
			?>
        </form>
    </div>
	<?php
}







