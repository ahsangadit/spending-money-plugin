<?php

class GoogleSpreadsheetsApi
{
	public function GetAccessToken( $client_id, $redirect_uri, $client_secret, $code )
	{
		$url = 'https://accounts.google.com/o/oauth2/token';

		$curlPost = 'client_id=' . $client_id . '&redirect_uri=' . $redirect_uri . '&client_secret=' . $client_secret . '&code=' . $code . '&grant_type=authorization_code';
		$ch       = curl_init();
		curl_setopt( $ch, CURLOPT_URL, $url );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $ch, CURLOPT_POST, 1 );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $curlPost );
		$data      = json_decode( curl_exec( $ch ), true );
		$http_code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
		if ( $http_code != 200 ) {
			throw new Exception( 'Error : Failed to receieve access token' );
		}

		return $data;
	}

	public function CreateSpreadsheet( $spreadsheet_title, $access_token )
	{
		$curlPost = array( 'properties' => array( 'title' => $spreadsheet_title ) );

		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, 'https://sheets.googleapis.com/v4/spreadsheets' );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $ch, CURLOPT_POST, 1 );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer ' . $access_token,
			'Content-Type: application/json'
		) );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $curlPost ) );
		$data = json_decode( curl_exec( $ch ), true );
		$http_code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
		if ( $http_code != 200 ) {
			throw new Exception( 'Error : Failed to create spreadsheet' );
		}

		return array( 'spreadsheet_id' => $data['spreadsheetId'], 'spreadsheet_url' => $data['spreadsheetUrl'] );
	}

	public function UpdateSpreadsheet( $spreadsheet_id, $access_token, $data )
	{
		$curlPost = $data;
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, 'https://sheets.googleapis.com/v4/spreadsheets/' . $spreadsheet_id . '/values/' . $curlPost['range'] . '?valueInputOption=USER_ENTERED' );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'PUT' );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer ' . $access_token,
			'Content-Type: application/json'
		) );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $curlPost ) );
		$data = json_decode( curl_exec( $ch ), true );
		$http_code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
		if ( $http_code != 200 ) {
			if ( ! empty( $data['error'] ) ) {
				throw new Exception( 'Error : ' . $data['error']['message'] );
			} else {
				throw new Exception( 'Error : Failed to update sheets' );
			}
		}

		return array( 'spreadsheet_id' => $data['spreadsheetId'], 'data' => json_encode( $data ) );
	}
}

?>
