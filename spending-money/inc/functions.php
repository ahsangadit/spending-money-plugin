<?php

class SpendingMoney
{

	public function __construct()
	{
		session_start();
		include_once( SPENDINGMONEY_PLUGIN_DIR . 'inc/GoogleSpreadsheetsApi.php' );
		add_action( 'init', [ $this, 'register_post_types' ] );
		add_action( 'wp_ajax_spending_money', [ $this, 'prefix_ajax_spending_money' ] );
		add_action( 'wp_ajax_nopriv_spending_money', [ $this, 'prefix_ajax_spending_money' ] );
		add_shortcode( 'spend_shortCode', [ $this, 'shortCode_fun' ] );
		add_shortcode( 'bar_shortcode', [ $this, 'shortCode_bar' ] );
		add_action( 'save_post', [ $this, 'sheet_update' ], 10, 2 );


	}


	function new_blog_details_send( $post_id )
	{
		print_r( $post_id );
		die();
	}

	public function enqueue_scripts()
	{
		wp_enqueue_script( 'spend-js', plugins_url( 'assets/js/script.js', __FILE__ ), array( 'jquery' ), rand( 0, 9999 ), true );
		wp_enqueue_style( 'spend-css', plugins_url( 'assets/css/style.css', __FILE__ ) );

	}

	function register_post_types()
	{
		/**
		 * Post Type: Services.
		 */

		$labels = [
			"name"          => __( "Programs", "mytheme" ),
			"singular_name" => __( "Program", "mytheme" ),
		];

		$args = [
			"label"                 => __( "Programs", "mytheme" ),
			"labels"                => $labels,
			"description"           => "",
			"public"                => true,
			"publicly_queryable"    => false,
			"show_ui"               => true,
			"show_in_rest"          => true,
			"rest_base"             => "",
			"rest_controller_class" => "WP_REST_Posts_Controller",
			"has_archive"           => false,
			"show_in_menu"          => true,
			"show_in_nav_menus"     => true,
			"delete_with_user"      => false,
			"exclude_from_search"   => false,
			"capability_type"       => "post",
			"map_meta_cap"          => true,
			"hierarchical"          => false,
			"rewrite"               => [ "slug" => "program", "with_front" => true ],
			"query_var"             => true,
			"supports"              => [ "title" ],
		];

		register_post_type( "Programs", $args );


		/**
		 * Post Type: Services.
		 */

		$labels = [
			"name"          => __( "Form Details", "mytheme" ),
			"singular_name" => __( "Form Detail", "mytheme" ),
		];

		$args = [
			"label"                 => __( "Form Detail", "mytheme" ),
			"labels"                => $labels,
			"description"           => "",
			"public"                => true,
			"publicly_queryable"    => false,
			"show_ui"               => true,
			"show_in_rest"          => true,
			"rest_base"             => "",
			"rest_controller_class" => "WP_REST_Posts_Controller",
			"has_archive"           => false,
			"show_in_menu"          => true,
			"show_in_nav_menus"     => true,
			"delete_with_user"      => false,
			"exclude_from_search"   => false,
			"capability_type"       => "post",
			"map_meta_cap"          => true,
			"hierarchical"          => false,
			"rewrite"               => [ "slug" => "form-detail", "with_front" => true ],
			"query_var"             => true,
			"supports"              => [ "title", "editor" ],
		];

		register_post_type( "Form Details", $args );


		/**
		 * Post Type: Entries.
		 */

		$labels = [
			"name"          => __( "Entries", "mytheme" ),
			"singular_name" => __( "Entry", "mytheme" ),
		];

		$args = [
			"label"                 => __( "Entries", "mytheme" ),
			"labels"                => $labels,
			"description"           => "",
			"public"                => true,
			"publicly_queryable"    => false,
			"show_ui"               => true,
			"show_in_rest"          => true,
			"rest_base"             => "",
			"rest_controller_class" => "WP_REST_Posts_Controller",
			"has_archive"           => false,
			"show_in_menu"          => true,
			"show_in_nav_menus"     => true,
			"delete_with_user"      => false,
			"exclude_from_search"   => false,
			"capability_type"       => "post",
			"map_meta_cap"          => true,
			"hierarchical"          => false,
			"rewrite"               => [ "slug" => "entries", "with_front" => true ],
			"query_var"             => true,
			"supports"              => [ "title" ],
		];

		register_post_type( "Entries", $args );
	}


	function prefix_ajax_spending_money()
	{
		$programs     = $_POST['arr'];
		$form_id      = $_POST['form_id'];
		$form_name    = get_post( $form_id );
		$sheet_values = [];

		$my_post = array(
			'post_title'  => 'Entry',
			'post_status' => 'publish',
			'post_type'   => 'entries',
		);

		$entry_id = wp_insert_post( $my_post );

		foreach ( $programs as $data ) {
			update_post_meta( $entry_id, $data['key'], $data['value'] );
			array_push( $sheet_values, $data['value'] );
		}

		update_post_meta( $entry_id, 'form_id', $form_id );
		update_post_meta( $entry_id, 'form_name', $form_name->post_title );

		$cat_count      = get_post_meta( $form_id, 'spreadsheet_cat_count', true );
		$spreadsheet_id = get_post_meta( $form_id, 'spreadsheet_id' );
		$row_count      = get_post_meta( $form_id, 'spreadsheet_row_count', true );
		$letter         = $this->alphCount( count( $programs ) );
		$range          = 'Sheet1!A' . ( $row_count + 1 ) . ':' . $letter . ( $row_count + 1 );
		$sapi           = new GoogleSpreadsheetsApi();
		$access_token   = get_option( 'spendingMoney_access_token' );
		$data           = [
			"range"          => $range,
			"majorDimension" => "ROWS",
			"values"         => [
				$sheet_values
			],
		];

        $response       = [ 'status' => 'failed' ];

		if ( ! empty( $access_token ) ) {

			try {
				$sapi->UpdateSpreadsheet( $spreadsheet_id[0], $access_token, $data );
				update_post_meta( $form_id, 'spreadsheet_row_count', $row_count + 1 );

                $bar_page = get_option( 'bar_page' );
                $post     = get_post( $bar_page );
                $slug     = $post->post_name;
                $response = [ 'status' => 'success', 'entry_id' => $entry_id, 'slug' => $slug ];

			} catch ( Exception $e ) {
				$response['message'] = $e->getMessage();
			}
		}

		header( 'Content-Type: application/json' );
		echo json_encode( $response );
		die;
	}


	function shortCode_fun( $atts )
	{
		ob_start();
		?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,500;0,700;0,800;1,100;1,600;1,700&display=swap"
              rel="stylesheet">


        <style>
            @media (min-width: 768px) {
                .container1 .col-sm-4 {
                    float: left;
                }

                .container1 .col-sm-4 {
                    width: 33.33333333%;
                }
            }

            @media (max-width: 766px) {
                .circle-row {
                    margin: 0 !important;
                }

                .button-plus, .button-minus {
                    float: left;
                    margin: 0 5px !important;
                    color: white !important
                }

                .input-group {
                    margin: 0 auto;
                    width: 195px;
                }

                input.quantity-field {
                    top: 0;
                    margin: 0;
                }

            }

            .button-plus, .button-minus {
                color: white !important
            }

            button, input[type="button"], input[type="reset"], input[type="submit"], #infinite-handle span {
                padding: 4px 13px 4px !important;
            }

            .container1 .col-sm-4 {
                position: relative;
                min-height: 1px;
                padding-right: 15px;
                padding-left: 15px;
            }


            .container1 h2 {
                font-family: inherit;
                font-weight: 500;
                line-height: 1.1;
                color: inherit;
            }

            .container1 .h2 .small {
                font-weight: normal;
                line-height: 1;
                color: #777;
            }

            .container1 button,
            .container1 input,
            .container1 optgroup,
            .container1 select,
            .container1 textarea {
                margin: 0;
                font: inherit;
            }

            .container1 button {
                overflow: visible;
            }

            .container1button,
            .container1 select {
                text-transform: none;
            }

            .container1 button,
            .container1 html input[type="button"],
            .container1 input[type="reset"],
            .container1 input[type="submit"] {
                -webkit-appearance: button;
                cursor: pointer;
            }

            .container1 .row:before, .container1 .row:after {
                display: table;
                content: " ";
            }

            .container1 .row:after {
                clear: both;
            }

            .program-text {
                color: white;
                /*padding: 50px;*/
            }

            .coin-notselect {
                border-radius: 50%;
                padding: 15px 5px;
                margin-top: 20px !important;
            }

            .coin-select {
                border-radius: 50%;
                padding: 15px 5px;
                margin-top: 20px !important;
            }

            .program-row {
                text-align: center;
            }

            .circle-row {
                text-align: center;
            }

            .submit-row {
                text-align: center;
            }

            .submit-budget {
                background: #e64e4a;
                border: none;
                padding: 20px;
                color: white;
                border-radius: 10px;
                width: 250px;
            }

            .spending-money {
                width: 100%;
                max-width: 100%;
                margin: 0 auto;
                padding: 30px;
            }

            #spending_money_form {

            }

            .spending-money h2 {
                color: #0a3b60;
                font-weight: bold;
                font-size: 40px;
            }

            .spending-money p {
                font-size: 20px;
                color: white;
                margin-bottom: 0;
            }

            .container1 {
                max-width: 100%;
                width: 100%;
            }

            #spending-money-container {
                border-radius: 10px;
            }

            .spending-money input[type=submit] {
                color: #FFF;
            }

            input.quantity-field {
                width: 120px;
                height: 40px;
                position: relative;
                top: 2px;
                margin: 0 -5px;
            }

            .program-row b.program-text {
                color: #000;
                text-align: c;
                display: block;
                margin: 25px 0 4px 0;
            }

            .submit-row {
                margin: 31px 0 0 0;
                padding: 0 0 25px;
            }

            span.coin-select, .coin-notselect {
                display: inline-block;
                width: 60px;
                height: 60px;
                margin: 0 2px;
            }

            .input-group {
                display: block;
            }

            .program-text {
                color: white;
                font-size: 13px;
            }

            .program-row b.program-text {
                color: #fff;
            }

            body, h1, h2, h3, h4, h5, h6, p, a, span, button {
                font-family: 'Montserrat', sans-serif !Important;
            }


            button:hover, button:focus, input[type="button"]:hover, input[type="button"]:focus, input[type="reset"]:hover, input[type="reset"]:focus, input[type="submit"]:hover, input[type="submit"]:focus, #infinite-handle span:hover, #infinite-handle span:focus {
                top: 0;
            }


            @media only screen and (max-width: 600px) {
                .spending-money {
                    width: auto;
                }
            }

        </style>
		<?php

		$form          = get_post( $atts['form_id'] );
		$programs      = get_post_meta( $form->ID, 'programs', true );
		$total_points  = get_post_meta( $form->ID, 'total_points', true );
		$circle        = get_post_meta( $form->ID, 'no_of_points', true );
		$inc_buttons   = get_post_meta( $form->ID, 'inc_buttons', true );
		$coin_active   = get_post_meta( $form->ID, 'coin_active', true );
		$coin_inactive = get_post_meta( $form->ID, 'coin_inactive', true );
		$b_colour      = get_post_meta( $form->ID, 'b_colour', true );
		$submit_colour = get_post_meta( $form->ID, 'submit_colour', true );
		$one_value     = $total_points / $circle;
		$onee_value    = $total_points / $circle;
		$str           = (string) $onee_value;
		$str_length    = strlen( $str );
		if ( $str_length == 4 ) {
			$str = str_replace( '000', 'K', $str );
		}
		if ( $str_length == 5 ) {
			$str = substr_replace( $str, 'K', 2 );
		}
		if ( $str_length == 6 ) {
			$str = substr_replace( $str, 'K', 3 );
		}
		if ( $str_length == 7 ) {
			$str = substr_replace( $str, 'M', 1 );
		}
		$programs = unserialize( $programs );
		?>
        <style>

            .coin-notselect {
                background: <?php echo $coin_active; ?>;
            }

            .coin-select {
                background: <?php echo $coin_inactive; ?>;
            }

            .container1 {
                background: <?php echo $b_colour; ?>;
            }

            .button-plus, .button-minus {
                background-color: <?php echo $inc_buttons; ?> !Important;
                box-shadow: 0 4px <?php echo $inc_buttons; ?> !important;
            }

            .submit-budget {
                background: <?php echo $submit_colour; ?> !important;
                box-shadow: 0 4px <?php echo $submit_colour; ?> !important;
            }
            @media (max-width: 767px) {
                .button-plus, .button-minus {
                    box-shadow: 0 0px <?php echo $inc_buttons; ?> !important;
                }
                input.quantity-field{
                    width:100px
                }
                .input-group input.button-minus, .input-group input.button-plus {
                    top: 2px;
                    height: 40px;
                }
            }
        </style>
        <script>

            number_format = function (number, decimals, dec_point, thousands_sep) {
                number = number.toFixed(decimals);

                var nstr = number.toString();
                nstr += '';
                x = nstr.split('.');
                x1 = x[0];
                x2 = x.length > 1 ? dec_point + x[1] : '';
                var rgx = /(\d+)(\d{3})/;

                while (rgx.test(x1))
                    x1 = x1.replace(rgx, '$1' + thousands_sep + '$2');

                return x1 + x2;
            }

            function incrementValue(e) {
                e.preventDefault();
                var fieldName = $(e.target).data('field');
                var parent = $(e.target).closest('div');
                var currentVal = parent.find('input[name=' + fieldName + ']').val();
                var abc = currentVal.replace('$', '');
                var abc2 = abc.replace(/\,/g,'');
                //var currentVal = parseInt(abc);
                var currentVal = parseFloat(abc2);
                field_val = parent.find('input[name=' + fieldName + ']');

                if (!isNaN(currentVal)) {
                    num = currentVal + parseFloat("<?php echo $one_value ?>");
                    numLength = num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    console.log(numLength);

                    parent.find('input[name=' + fieldName + ']').val(currentVal + parseFloat("<?php echo $one_value ?>"));
                    var list = $(".coin-notselect");

                    $(list[0]).addClass('coin-select');
                    $(list[0]).removeClass('coin-notselect');
                  //alert(list.length)
                    if (list.length === 0) {
                       // alert(currentVal)
                        num2 =   currentVal;
                        numLength2 = num2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                        parent.find('input[name=' + fieldName + ']').val('$' + numLength2);
                    } else {
                        var cal = currentVal + parseFloat("<?php echo $one_value ?>");
                        //parent.find('input[name=' + fieldName + ']').val('$' + cal);
                        parent.find('input[name=' + fieldName + ']').val('$' + numLength);
                    }



                } else {
                    parent.find('input[name=' + fieldName + ']').val('$' + 0);
                }
            }

            function decrementValue(e) {
                e.preventDefault();
                var fieldName = $(e.target).data('field');
                var parent = $(e.target).closest('div');
                var currentVal = parent.find('input[name=' + fieldName + ']').val();
                var abc = currentVal.replace('$', '');
                var abc2 = abc.replace(/\,/g,'');
                var currentVal = parseInt(abc2);

                if (!isNaN(currentVal) && currentVal > 0) {

                    num = currentVal - parseFloat("<?php echo $one_value ?>");
                    numLength = num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                  // console.log(numLength);


                    var list = $(".coin-select");
                    length = list.length;
                    // alert(list.length)
                    $(list[length - 1]).addClass('coin-notselect');
                    $(list[length - 1]).removeClass('coin-select')
                    if (list.length === 0) {
                        num2 =  currentVal;
                        // alert(num2)
                        numLength2 = num2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                        parent.find('input[name=' + fieldName + ']').val('$' + numLength2);
                    } else {
                        var cal = currentVal - parseFloat("<?php echo $one_value ?>");
                        parent.find('input[name=' + fieldName + ']').val('$' + numLength);
                    }

                } else {
                    parent.find('input[name=' + fieldName + ']').val('$' + 0);
                }
            }

            $(window).on('load', function () {
                $('.button-plus').click(function (e) {

                    incrementValue(e);
               });

                $('.button-minus').click(function (e) {
                    decrementValue(e);
                });
            });

            $(document).ready(function () {
                $("#spending_money_form").submit(function (e) {
                    e.preventDefault();

                    var arr = [];

                    $('.quantity-field').each(function () {
                        var key = $(this).attr('data-program');
                        var value = $(this).val();

                        arr.push({"key": key, "value": value});
                    });

                    var form_id = $('.form_id').val();
                    var btn = $(this).find('input[type="submit"]');
                    var qtys = 0;
                    var actionn = 'spending_money';
                    var data;
                    var total = parseFloat($(this).find('input[name="ttl"]').val());
                    $(this).find('.quantity-field').each(function () {
                        var rep_dol = $(this).val().replace('$', '');
                         var rep_comm = rep_dol.replace(',', '');
                        qtys += parseFloat(rep_comm);
                    });
                    // alert(qtys);
                    // alert(total);

                    if (qtys !== total) {
                        alert("Please select all coins!");
                        return;
                    }

                    btn.attr('disabled', 'disabled');
                    btn.val('Please wait...');

                    jQuery.ajax({
                        type: 'post',
                        url: '/wp-admin/admin-ajax.php',
                        dataType: 'JSON',
                        data: {
                            action: actionn,
                            arr: arr,
                            form_id: form_id,
                        },
                        success: function (j) {
                            btn.removeAttr('disabled');

                            var entry_id = j.entry_id;
                            var slug = j.slug;

                            if (j.status === "success") {
                                window.location.href = ("<?= get_bloginfo( 'url' ) ?>/" + slug + "/?entry_id=" + entry_id);
                            } else {
                                alert(j.message);
                            }
                        },
                        error: function (data) {
                            console.log("11" + data);
                        }

                    });
                });
            });


        </script>

        <div id="spending-money-container" class="container1">

            <div class="spending-money" style="text-align:center;">
                <h2><?= $form->post_title ?></h2>
                <p><?php echo $form->post_content; ?></p>
            </div>
            <form method="post" id="spending_money_form">
                <div class="row circle-row">
					<?php for ( $i = 1; $i <= $circle; $i ++ ) { ?>
                        <span class="coin-notselect">$<?= $str ?></span>
					<?php } ?>
                </div>

                <div class="row program-row">
					<?php foreach ( $programs as $data ) {
						$post_program = get_post( $data ); ?>
                        <div class="col-sm-4">
							<?php echo "<b class='program-text'>" . $post_program->post_title . "</b>" ?>
                            <div class="input-group">
                                <input type="button" value="-" class="button-minus" data-field="quantity">
                                <input type="text" step="1" max="" value="$0" name="quantity"
                                       data-program="<?= $post_program->post_title ?>" class="quantity-field" readonly>
                                <input type="button" value="+" class="button-plus" data-field="quantity">
                            </div>
                        </div>
					<?php } ?>
                    <input type="hidden" value="<?= $form->ID ?>" name="form_id" class="form_id">
                    <input type="hidden" value="<?= ( $circle * $onee_value ) ?>" name="ttl"/>
                </div>
                <div class="row submit-row">
                    <input type="submit" class="submit-budget" value="SUBMIT YOUR BUDGET"/>
                </div>
            </form>
        </div>
		<?php
		$output = ob_get_clean();

		return $output;
	}

	public function shortCode_bar()
	{
		ob_start();
		?>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.js"></script>


        <style>
            .vote-us {
                color: #303e5b;
                text-align: center;
            }

            .community {
                color: white;
                text-align: center;
            }

            #canvas-width {
                width: 600px;
                margin: 0 auto;
            }
        </style>

		<?php
		$entry_id = $_GET['entry_id'];
		$form_id  = get_post_meta( $entry_id, 'form_id', true );
		$args     = array(
			'post_type'      => 'entries',
			'post_status'    => 'publish',
			'posts_per_page' => - 1,
			'meta_query'     => array(
				array(
					'key'     => 'form_id',
					'compare' => '=',
					'value'   => $form_id,
				),
			)
		);

		$entries = get_posts( $args );
		$program = [];

		foreach ( $entries as $entry ) {
			$entry_id = $entry->ID;

			$args          = array(
				'post_type'      => 'programs',
				'post_status'    => 'publish',
				'posts_per_page' => - 1,
			);
			$programs      = get_posts( $args );
			$form_programs = get_post_meta( $form_id, 'programs', true );
			$form_programs = unserialize( $form_programs );

			if ( empty( $form_programs ) ) {
				$form_programs = [];
			}

			foreach ( $programs as $key => $data ) {
				if ( ! in_array( $data->ID, $form_programs ) ) {
					continue;
				}
				$value = get_post_meta( $entry_id, $data->post_title, true );

				$modi_points = intval( str_replace( "$", "", $value ) );
                $modi_points1= intval( str_replace( ",", "", $modi_points ) );

				if ( ! isset( $program[ $key ]['points'] ) ) {
					$program[ $key ]['points'] = 0;
				}

				$program[ $key ]['points'] += $modi_points1;
				$colour                    = get_post_meta( $data->ID, 'colour', true );
				$program[ $key ]["color"]  = $colour;
				$program[ $key ]["label"]  = $data->post_title;
			}
		}

		$labels = [];
		$points = [];
		$color  = [];
		foreach ( $program as $data ) {
			array_push( $labels, $data['label'] );
		}

		foreach ( $program as $data ) {
			array_push( $points, $data['points'] );
		}

		foreach ( $program as $data ) {
			array_push( $color, $data['color'] );
		}


		$rr = array_values( $points );

		$labels_array = json_encode( array_values( $labels ) );


        //$labels_array = json_encode( $labels  );


		$points_array = json_encode( array_values( $points ) );
		$color_array  = json_encode( array_values( $color ) );
//var_dump($points_array);
		?>
<style>


</style>


        <div class="container">
            <div class="row">
                <h2 class="vote-us">Thank you for submitting your feedback! </h2>
            </div>
            <div class="row">
                <h2 class="community">This is how your community is voting so far: </h2>
            </div>
            <div class="row">
                <div class="container" style="  position: relative;margin: auto;height: 80vh;">
                        <canvas id="canvas"></canvas>
                </div>
            </div>
            </div>
        </div>

        <script>

      var data = {
          labels: <?php echo $labels_array; ?>,
          datasets: [{
                    label:"Points",
                    backgroundColor: <? echo $color_array?>,
                    borderColor: <? echo $color_array?>,
                    borderWidth: 2,
                    hoverBackgroundColor: <? echo $color_array?>,
                    hoverBorderColor: <? echo $color_array?>,
                    data: <? echo $points_array?>,

                }]
            };

            var options = {
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                        ticks: {
                            display: false
                        },
                        stacked: true,
                        gridLines: {
                            display: false,
                            drawBorder: false,
                            color: "rgba(255,99,132,0.2)"
                        }
                    }],
                    xAxes: [{
                        gridLines: {
                            display: false,
                            drawBorder: false,
                        }
                    }]
                }
            };

            Chart.Bar('canvas', {
                options: options,
                data: data
            });
        </script>

		<?php
		$output = ob_get_clean();

		return $output;
	}

	function sheet_create( $post_id )
	{

		$form = get_post( $post_id );

		$sapi         = new GoogleSpreadsheetsApi();
		$access_token = get_option( 'spendingMoney_access_token' );
		$create       = $sapi->CreateSpreadsheet( $form->post_title, $access_token );
		update_post_meta( $post_id, 'spreadsheet_id', $create['spreadsheet_id'] );
		update_post_meta( $post_id, 'spreadsheet_url', $create['spreadsheet_url'] );
		update_post_meta( $post_id, 'spreadsheet_cat_count', 0 );

	}

	function alphCount( $num )
	{
		$arr = [
			1  => 'A',
			2  => 'B',
			3  => 'C',
			4  => 'D',
			5  => 'E',
			6  => 'F',
			7  => 'G',
			8  => 'H',
			9  => 'I',
			10 => 'J'

		];

		return $arr[ $num ];
	}

	function sheet_update( $post_id, $post )
	{
		if ( ! empty( $post ) && $post->post_status !== 'auto-draft' && get_post_type( $post_id ) == 'formdetails' ) {
			if ( $post->post_status == 'publish' ) {

				$spreadsheet_id = get_post_meta( $post_id, 'spreadsheet_id' );
				if ( ! $spreadsheet_id ) {
					$this->sheet_create( $_POST['post_ID'] );
				}
				$cat_count = get_post_meta( $post_id, 'spreadsheet_cat_count', true );
				$row_count = get_post_meta( $post_id, 'spreadsheet_row_count' );
				$programs  = get_post_meta( $post_id, 'programs', true );
				$programs  = unserialize( $programs );
				if ( count( $programs ) != $cat_count ) {
					$spreadsheet_id = get_post_meta( $post_id, 'spreadsheet_id', true );

					$program_arr = [];
					$arr         = [];
					for ( $i = 0; $i <= 9; $i ++ ) {
						if ( isset( $programs[ $i ] ) ) {
							$post       = get_post( $programs[ $i ] );
							$post_title = $post->post_title;
						} else {
							$post_title = " ";
						}
						array_push( $program_arr, $post_title );
					}

					$letter = $this->alphCount( count( $programs ) );
					$range  = 'Sheet1!A1:J1';

					$sapi = new GoogleSpreadsheetsApi();

					$access_token = get_option( 'spendingMoney_access_token' );
					$data         = [
						"range"          => $range,
						"majorDimension" => "ROWS",
						"values"         => [
							$program_arr
						],
					];

					$updata = $sapi->UpdateSpreadsheet( $spreadsheet_id, $access_token, $data );
					update_post_meta( $post_id, 'spreadsheet_cat_count', count( $program_arr ) );
					if ( ! $row_count ) {
						update_post_meta( $post_id, 'spreadsheet_row_count', 1 );
					}
				}

			}
			$programs = get_post_meta( $post_id, 'programs', true );
			$programs = unserialize( $programs );
			update_post_meta( $post_id, 'spreadsheet_cat_count', count( $programs ) );
		}

	}


}

new SpendingMoney();
